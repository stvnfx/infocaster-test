# Age calculator app - Infocaster

# Stappenplan

1. Clone de repo in een folder naar keuze
2. Zorg dat je op node 18 zit (nvm)
3. Run npm i om alle packages te installeren
4. Run npm run dev om het project te starten

# Wat willen we zien

De opdracht is om een Age calculator te maken die zoveel mogelijk in de buurt komt van de afbeeldingen in de design folder.

De gebruiker moet het volgende kunnen doen:

- Een leeftijd in jaren, maanden en dagen bekijken na het submitten van een validate datum
- Validatie regels:
  - Alle velden moeten ingevuld zijn
  - De dag moet een nummer tussen 1-31 zijn
  - De maand moet een nummer tussen 1-12 zijn
  - De datum mag niet in de toekomst liggen
- Interactie kunnen zien bij bv. hover of focus states

# Styleguide

## Layout

Designs zijn gemaakt met de volgende grids:

- Mobile: 375px
- Desktop: 1440px

## Colors

### Primary

- Purple: hsl(259, 100%, 65%)
- Light red: hsl(0, 100%, 67%)

### Neutral

- White: hsl(0, 0%, 100%)
- Off white: hsl(0, 0%, 94%)
- Light grey: hsl(0, 0%, 86%)
- Smokey grey: hsl(0, 1%, 44%)
- Off black: hsl(0, 0%, 8%)

## Typography

### Body Copy

- Font size (inputs): 32px

### Font

- Family: [Poppins](https://fonts.google.com/specimen/Poppins)
- Weights: 400i, 700, 800i
